# General-Template

This repository contains the general template for all CoScInE projects.

It includes the following features:

* Default files (LICENSE, .gitlab-ci.yml, etc.)
* Default project structure
* Own documentation process